package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;

public class DagligSkaevTest {
	private Laegemiddel lm;
	private DagligSkaev dagligSkaev;
	
	@Test
	public void samletDosisTC1()
	{
		lm = new Laegemiddel("FloedeIs", 1, 2, 3, "ml");
		dagligSkaev = new DagligSkaev(LocalDate.now(), LocalDate.now().plusDays(4), lm);
		dagligSkaev.createDosis(LocalTime.of(8, 0), 30);
		dagligSkaev.createDosis(LocalTime.of(12, 0), 40);
		dagligSkaev.createDosis(LocalTime.of(18, 0), 50);
		dagligSkaev.createDosis(LocalTime.of(2, 0), 40);
		assertEquals(800, dagligSkaev.samletDosis(), 0.01);
	}
	
	@Test
	public void doegnDosisTC1()
	{
		lm = new Laegemiddel("FloedeIs", 1, 2, 3, "ml");
		dagligSkaev = new DagligSkaev(LocalDate.now(), LocalDate.now().plusDays(4), lm);
		dagligSkaev.createDosis(LocalTime.of(8, 0), 30);
		dagligSkaev.createDosis(LocalTime.of(12, 0), 40);
		dagligSkaev.createDosis(LocalTime.of(18, 0), 50);
		dagligSkaev.createDosis(LocalTime.of(2, 0), 40);
		assertEquals(160, dagligSkaev.doegnDosis(), 0.01);
	}

}
