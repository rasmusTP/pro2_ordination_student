package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;
import storage.Storage;

public class ServiceTest {
	private Service service;
	private Laegemiddel lm;
	private Patient patient;	
	
	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		lm = new Laegemiddel("bacon", 2, 4, 8, "Stk.");
		patient = new Patient("Børge", "Børgesen", 95);
	}
	
	@Test
	public void OpretPnOrdinationTC1() {
		assertNotNull(service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 50));
		assertNotNull(patient.getOrdinationer().get(0));
	}
	
	@Test
	public void OpretPnOrdinationTC2() {
		assertNotNull(service.opretPNOrdination(LocalDate.now(), LocalDate.now(), patient, lm, 50));
		assertNotNull(patient.getOrdinationer().get(0));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretPnOrdinationTC3() {
		assertNull(service.opretPNOrdination(LocalDate.now(), LocalDate.now().minusDays(4), patient, lm, 50));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretPnOrdinationTC4() {
		assertNull(service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, null, 50));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretPnOrdinationTC5() {
		assertNull(service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), null, lm, 50));
	}
	
	@Test
	public void OpretDagligFastTC1() {
		DagligFast df =  service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 30,	30,	30,	40);	
		assertNotNull(df);
		assertEquals(df, patient.getOrdinationer().get(0));
		
		double[] actual = {df.getDosis()[0].getAntal(), df.getDosis()[1].getAntal(), df.getDosis()[2].getAntal(), df.getDosis()[3].getAntal()};
		
		assertArrayEquals(new double[] {30.0, 30.0, 30.0, 40.0} , actual, 0.001);
		/*
		assertEquals(30, df.getBestemtDosis(0).getAntal(), 0.001);
		assertEquals(30, df.getBestemtDosis(1).getAntal(), 0.001);
		assertEquals(30, df.getBestemtDosis(2).getAntal(), 0.001);
		assertEquals(40, df.getBestemtDosis(3).getAntal(), 0.001);
		*/		
	}
	
	@Test
	public void OpretDagligFastTC2() {
		DagligFast df =  service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now(), patient, lm, 30,	30,	30,	40);	
		assertNotNull(df);
		assertEquals(df, patient.getOrdinationer().get(0));
		
		double[] actual = {df.getDosis()[0].getAntal(), df.getDosis()[1].getAntal(), df.getDosis()[2].getAntal(), df.getDosis()[3].getAntal()};
		
		assertArrayEquals(new double[] {30.0, 30.0, 30.0, 40.0} , actual, 0.001);	
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretDagligFastTC3() {
		DagligFast df =  service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().minusDays(4), patient, lm, 30,	30,	30,	40);	
		assertNotNull(df);
		
		double[] actual = {df.getDosis()[0].getAntal(), df.getDosis()[1].getAntal(), df.getDosis()[2].getAntal(), df.getDosis()[3].getAntal()};
		
		assertArrayEquals(new double[] {30.0, 30.0, 30.0, 40.0} , actual, 0.001);	
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretDagligFastTC4() {
		DagligFast df =  service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, null, 30,	30,	30,	40);	
		assertNotNull(df);
		
		double[] actual = {df.getDosis()[0].getAntal(), df.getDosis()[1].getAntal(), df.getDosis()[2].getAntal(), df.getDosis()[3].getAntal()};
		
		assertArrayEquals(new double[] {30.0, 30.0, 30.0, 40.0} , actual, 0.001);	
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretDagligFastTC5() {
		DagligFast df =  service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), null, lm, 30,	30,	30,	40);	
		assertNotNull(df);
		
		double[] actual = {df.getDosis()[0].getAntal(), df.getDosis()[1].getAntal(), df.getDosis()[2].getAntal(), df.getDosis()[3].getAntal()};
		
		assertArrayEquals(new double[] {30.0, 30.0, 30.0, 40.0} , actual, 0.001);	
	}
	
	@Test
	public void OpretDagligFastTC6() {
		DagligFast df =  service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 0, 30, 30, 40);	
		assertNotNull(df);
		assertEquals(df, patient.getOrdinationer().get(0));
		
		double[] actual = {df.getDosis()[0].getAntal(), df.getDosis()[1].getAntal(), df.getDosis()[2].getAntal(), df.getDosis()[3].getAntal()};
		
		assertArrayEquals(new double[] {0.0, 30.0, 30.0, 40.0} , actual, 0.001);		
	}
	
	@Test
	public void OpretDagligSkaevTC1() {
		LocalTime[] tider = {LocalTime.of(10, 00), LocalTime.of(15, 00), LocalTime.of(18, 00)};
		double antal[] = {50.0, 50.0, 50.0};
		ArrayList<Dosis> dosis = new ArrayList<Dosis>();
		Dosis d1 = new Dosis(LocalTime.of(10, 00), 50.0);
		Dosis d2 = new Dosis(LocalTime.of(15, 00), 50.0);
		Dosis d3 = new Dosis(LocalTime.of(18, 00), 50.0);
		dosis.add(d1);
		dosis.add(d2);
		dosis.add(d3);
		
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, tider, antal);
		System.out.println(ds.getDosis());
		System.out.println(dosis);
		
		assertNotNull(ds);
		assertEquals(ds, patient.getOrdinationer().get(0));
		assertEquals(dosis, ds.getDosis());
	}
	
	@Test
	public void OpretDagligSkaevTC2() {
		LocalTime[] tider = {LocalTime.of(10, 00), LocalTime.of(15, 00), LocalTime.of(18, 00)};
		double antal[] = {50.0, 50.0, 50.0};
		ArrayList<Dosis> dosis = new ArrayList<Dosis>();
		Dosis d1 = new Dosis(LocalTime.of(10, 00), 50.0);
		Dosis d2 = new Dosis(LocalTime.of(15, 00), 50.0);
		Dosis d3 = new Dosis(LocalTime.of(18, 00), 50.0);
		dosis.add(d1);
		dosis.add(d2);
		dosis.add(d3);
		
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now(), patient, lm, tider, antal);
		
		assertNotNull(ds);
		assertEquals(ds, patient.getOrdinationer().get(0));
		assertEquals(dosis, ds.getDosis());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretDagligSkaevTC3() {
		LocalTime[] tider = {LocalTime.of(10, 00), LocalTime.of(15, 00), LocalTime.of(18, 00)};
		double antal[] = {50.0, 50.0, 50.0};
		ArrayList<Dosis> dosis = new ArrayList<Dosis>();
		Dosis d1 = new Dosis(LocalTime.of(10, 00), 50.0);
		Dosis d2 = new Dosis(LocalTime.of(15, 00), 50.0);
		Dosis d3 = new Dosis(LocalTime.of(18, 00), 50.0);
		dosis.add(d1);
		dosis.add(d2);
		dosis.add(d3);
		
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().minusDays(4), patient, lm, tider, antal);
		
		assertNull(ds);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretDagligSkaevTC4() {
		LocalTime[] tider = {LocalTime.of(10, 00), LocalTime.of(15, 00), LocalTime.of(18, 00)};
		double antal[] = {50.0, 50.0, 50.0};
		ArrayList<Dosis> dosis = new ArrayList<Dosis>();
		Dosis d1 = new Dosis(LocalTime.of(10, 00), 50.0);
		Dosis d2 = new Dosis(LocalTime.of(15, 00), 50.0);
		Dosis d3 = new Dosis(LocalTime.of(18, 00), 50.0);
		dosis.add(d1);
		dosis.add(d2);
		dosis.add(d3);
		
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, null, tider, antal);
		
		assertNull(ds);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void OpretDagligSkaevTC5() {
		LocalTime[] tider = {LocalTime.of(10, 00), LocalTime.of(15, 00), LocalTime.of(18, 00)};
		double antal[] = {50.0, 50.0, 50.0};
		ArrayList<Dosis> dosis = new ArrayList<Dosis>();
		Dosis d1 = new Dosis(LocalTime.of(10, 00), 50.0);
		Dosis d2 = new Dosis(LocalTime.of(15, 00), 50.0);
		Dosis d3 = new Dosis(LocalTime.of(18, 00), 50.0);
		dosis.add(d1);
		dosis.add(d2);
		dosis.add(d3);
		
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.now(), LocalDate.now().plusDays(4), null, lm, tider, antal);
		
		assertNull(ds);
	}

	@Test
	public void ordinationPNAnvendtTC1() {
		PN pn = service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 50);
		service.ordinationPNAnvendt(pn, LocalDate.now().plusDays(1));	
		assertEquals(LocalDate.now().plusDays(1), pn.getDatoer().get(0));
	}
	
	@Test
	public void ordinationPNAnvendtTC2() {
		PN pn = service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 50);
		service.ordinationPNAnvendt(pn, LocalDate.now());	
		assertEquals(LocalDate.now(), pn.getDatoer().get(0));
	}
	
	@Test //(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendtTC3() {
		PN pn = service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 50);
		
		try {
			service.ordinationPNAnvendt(pn, LocalDate.now().plusDays(5));
			//fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Datoen er ikke indenfor ordinationens gyldighedsperiode");
		}
	}
	
	@Test //(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendtTC4() {
		PN pn = service.opretPNOrdination(LocalDate.now(), LocalDate.now().plusDays(4), patient, lm, 50);
		
		try {
			service.ordinationPNAnvendt(pn, LocalDate.now().minusDays(1));
			//fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Datoen er ikke indenfor ordinationens gyldighedsperiode");
		}
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC1() {
		Laegemiddel lm = service.opretLaegemiddel("Bacon", 2, 4, 8, "stk.");
		Patient p = service.opretPatient("32891678", "Børge", 90);
		
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p, lm, 30, 30, 30, 40);
		
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(0.0, 1000.0, lm));
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC2() {
		Laegemiddel lm = service.opretLaegemiddel("Bacon", 2, 4, 8, "stk.");
		Patient p = service.opretPatient("32891678", "Børge", 90);
		
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p, lm, 30, 30, 30, 40);
		
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(0, 25, lm));
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC3() {
		Laegemiddel lm = service.opretLaegemiddel("Bacon", 2, 4, 8, "stk.");
		Patient p = service.opretPatient("32891678", "Børge", 90);
		Patient p2 = service.opretPatient("32191678", "Egon", 60);
		
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p, lm, 30, 30, 30, 40);
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p2, lm, 30, 30, 30, 40);
		
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(25, 60, lm));
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC4() {
		Laegemiddel lm = service.opretLaegemiddel("Bacon", 2, 4, 8, "stk.");
		Patient p = service.opretPatient("32891678", "Børge", 90);
		Patient p2 = service.opretPatient("32191678", "Egon", 60);
		
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p, lm, 30, 30, 30, 40);
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p2, lm, 30, 30, 30, 40);
		
		assertEquals(2, service.antalOrdinationerPrVægtPrLægemiddel(60, 120, lm));
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC5() {
		Laegemiddel lm = service.opretLaegemiddel("Bacon", 2, 4, 8, "stk.");
		Patient p = service.opretPatient("32891678", "Børge", 90);
		Patient p2 = service.opretPatient("32191678", "Egon", 60);
		
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p, lm, 30, 30, 30, 40);
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p2, lm, 30, 30, 30, 40);
		
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(120, 180, lm));
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTC6() {
		Laegemiddel lm = service.opretLaegemiddel("Bacon", 2, 4, 8, "stk.");
		Patient p = service.opretPatient("32891678", "Børge", 90);
		Patient p2 = service.opretPatient("32191678", "Egon", 60);
		
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p, lm, 30, 30, 30, 40);
		service.opretDagligFastOrdination(LocalDate.now(), LocalDate.now().plusDays(4), p2, lm, 30, 30, 30, 40);
		
		try {
			assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(80, 65, lm));
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Start vægt skal være mindre en slut vægt");
		}
		
	}
}
