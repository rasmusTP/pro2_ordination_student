package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;

public class DagligFastTest {
	private Laegemiddel lm;
	private DagligFast dagligfast;
	
	@Test
	public void samletDosisTC1()
	{
		lm = new Laegemiddel("FloedeIs", 1, 2, 3, "ml");
		dagligfast = new DagligFast(LocalDate.now(), LocalDate.now().plusDays(4), lm);
		dagligfast.createDosis(LocalTime.of(8, 0), 30, 0);
		dagligfast.createDosis(LocalTime.of(12, 0), 40, 1);
		dagligfast.createDosis(LocalTime.of(18, 0), 50, 2);
		dagligfast.createDosis(LocalTime.of(2, 0), 40, 3);
		assertEquals(800, dagligfast.samletDosis(), 0.01);
	}
	
	@Test
	public void doegnDosisTC1()
	{
		lm = new Laegemiddel("FloedeIs", 1, 2, 3, "ml");
		dagligfast = new DagligFast(LocalDate.now(), LocalDate.now().plusDays(4), lm);
		dagligfast.createDosis(LocalTime.of(8, 0), 30, 0);
		dagligfast.createDosis(LocalTime.of(12, 0), 40, 1);
		dagligfast.createDosis(LocalTime.of(18, 0), 50, 2);
		dagligfast.createDosis(LocalTime.of(2, 0), 40, 3);
		assertEquals(160, dagligfast.doegnDosis(), 0.01);
	}
	
	//meh

}
