package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {
	private Laegemiddel lm;
	private PN pn;
	
	@Test
	public void givDosisTC1()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(), LocalDate.now().plusDays(3), lm, 2);
		assertTrue(pn.givDosis(LocalDate.now()));
		assertEquals(1, pn.getAntalGangeGivet());
	}
	
	@Test 
	public void givDosisTC2()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(), LocalDate.now().plusDays(3), lm, 2);
		assertFalse(pn.givDosis(LocalDate.now().minusDays(1)));
	}
	
	@Test
	public void givDosisTC3()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(), LocalDate.now().plusDays(3), lm, 2);
		assertTrue(pn.givDosis(LocalDate.now().plusDays(3)));
		assertEquals(1, pn.getAntalGangeGivet());
	}
	
	@Test 
	public void givDosisTC4()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(), LocalDate.now().plusDays(3), lm, 2);
		assertFalse(pn.givDosis(LocalDate.now().plusDays(5)));
	}
	
	@Test
	public void givDosisTC5()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(), LocalDate.now().plusDays(3), lm, 2);
		assertTrue(pn.givDosis(LocalDate.now().plusDays(2)));
		assertEquals(1, pn.getAntalGangeGivet());
	}
	
	@Test
	public void samletDosisTC1()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(),LocalDate.now().plusDays(3), lm, 2);
		pn.givDosis(LocalDate.now());
		pn.givDosis(LocalDate.now());
		pn.givDosis(LocalDate.now().plusDays(1));
		pn.givDosis(LocalDate.now().plusDays(1));
		pn.givDosis(LocalDate.now().plusDays(2));
		pn.givDosis(LocalDate.now().plusDays(2));
		pn.givDosis(LocalDate.now().plusDays(3));
		assertEquals(14, pn.samletDosis(), 0.001);
		
	}
	
	@Test
	public void doegnDosis()
	{
		lm = new Laegemiddel("Halspastil", 1, 2, 3, "stk");
		pn = new PN(LocalDate.now(),LocalDate.now().plusDays(3), lm, 2);
		pn.givDosis(LocalDate.now());
		pn.givDosis(LocalDate.now());
		pn.givDosis(LocalDate.now().plusDays(1));
		pn.givDosis(LocalDate.now().plusDays(1));
		pn.givDosis(LocalDate.now().plusDays(2));
		pn.givDosis(LocalDate.now().plusDays(2));
		pn.givDosis(LocalDate.now().plusDays(3));
		assertEquals(3.5, pn.doegnDosis(), 0.01);
	}
	

}
