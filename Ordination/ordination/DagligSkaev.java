package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosisListe = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel lm) {
		super(startDen, slutDen, lm);
	}

	public void createDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		addDosis(d);       
	}

	public void addDosis(Dosis dosis)
	{
		dosisListe.add(dosis);
	}

	public void removeDosis(Dosis dosis)
	{
		dosisListe.remove(dosis);
	}
	
	public ArrayList<Dosis> getDosis() {
		return dosisListe;
	}


	@Override
	public double samletDosis() {
		double samletDosis = 0;

		for (Dosis d : dosisListe) {
			samletDosis += d.getAntal();
		}

		return samletDosis * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / super.antalDage();
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}
}
