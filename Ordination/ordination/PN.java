package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {
	private ArrayList<LocalDate> datoer;
	private double antalEnheder;
	private int antalGivet;
	
	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel lm, double antalEnheder) {
		super(startDen, slutDen, lm);
		this.antalEnheder = antalEnheder;
		this.datoer = new ArrayList<>();
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen
	 * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		LocalDate startDato = super.getStartDen();
		LocalDate slutDato = super.getSlutDen();

		if (givesDen.equals(startDato) || givesDen.isAfter(startDato) && givesDen.isBefore(slutDato) || givesDen.isEqual(slutDato)) {
			this.datoer.add(givesDen);
			return true;
		} else
			return false;  
	}
	
	public ArrayList<LocalDate> getDatoer() {
		return this.datoer;
	}

	public double doegnDosis() {
		//return (datoer.size() * antalEnheder) / super.antalDage();
		return samletDosis() / super.antalDage();
	}

	
	public double samletDosis() {
		return datoer.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoer.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

}
