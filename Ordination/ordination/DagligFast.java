package ordination;

import java.time.*;

public class DagligFast extends Ordination {
	private Dosis[] dosisListe = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel lm) {
		super(startDen, slutDen, lm);
	}

	public Dosis createDosis(LocalTime tid, double antal, int index) {
		Dosis d = new Dosis(tid, antal);
		addDosis(d, index);
		return d;
	}

	public Dosis[] getDosis() {
		return this.dosisListe;
	}
	
	public Dosis getBestemtDosis(int index) {
		return dosisListe[index];
	}

	public void addDosis(Dosis dosis, int index) {
		this.dosisListe[index] = dosis;
	}

	public void setDosis(Dosis[] dosis) {
		this.dosisListe = dosis;
	}

	public void removeDosis(int index) {
		dosisListe[index] = null;
	}

	@Override
	public double samletDosis() {				
		
		return doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {		
		double antal = 0;
		
		for (int i = 0; i < dosisListe.length; i++) {
			antal += dosisListe[i].getAntal();
		}
		
		return antal;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return this.getClass().getSimpleName();
	}
}
